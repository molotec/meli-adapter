const request = require('request-promise-native');

/**
 * Get products and information from MercadoLibre user
 *
 * @param nick MercadoLibre user nickname
 */
const getProducts = (nick) => {
    return request({
        url: 'https://api.mercadolibre.com/sites/MLA/search',
        qs: {
            nickname: nick
        },
        json: true
    })
};

/**
 * Get description about a single item
 *
 * @param id MercadoLibre item ID
 */
const getDescription = (id) => {
    return request({
        url: 'https://api.mercadolibre.com/items/' + id + '/description',
        json: true
    })
};

const getProductsItems = (response) => {
    return response.results;
};


/**
 * Returns a new product object only with selected information
 *
 * @param product
 */
const simplifyProduct = (product) => {
    return {
        id: product.id,
        price: product.price,
        title: product.title,
        permalink: product.permalink,
    }
};

const simplifyAllProducts = (products) => {
    return products.map(simplifyProduct);
};

// Executes the sequence only if username provided
if (process.argv[2]) {
    getProducts(process.argv[2])
        .then(getProductsItems)
        .then(simplifyAllProducts)
        .then(console.log)
} else {
    console.log("No username provided");
}